# 1. go to https://overpass-turbo.eu/
# 2. navigate the map to the area of interest. In this example, it would be the twin cities metro area.
# 3. change the query in the left panel to:
# node
#   [highway=traffic_signals]
#   ({{bbox}});
# out;
# 4. click on Run
# 5. click on Export > Data > download as GeoJSON and save the downloaded file into Data/ folder
# 6. turn it into an RDS file by calling the function below

geojson_to_rds <- function(path = "Data/export.geojson", layer = "export") {
  signals <- readOGR(dsn = path, layer = layer)
  saveRDS(signals, "Data/traffic_signals.RDS")
}
# geojson_to_rds(path = "Data/export.geojson", layer = "export")