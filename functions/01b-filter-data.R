# inputs:
  ## msgs: output of clean_lm() 
  ## min_date: minimum date (yyyy-mm-dd)
  ## max_date: maximum date (yyyy-mm-dd)
  ## min_hour: minimum hour (num)
  ## max_hour: maximum hour (num)
  ## schedule: one or more of Weekday, Saturday, Sunday, Holiday, Reduced
# output:
  ## filtered msgs

filter_lm <- function(msgs, min_date, max_date, min_hour, max_hour, schedule = c("Weekday", "Saturday", "Sunday", "Holiday", "Reduced")) {
  return(msgs[CALENDAR_ID >= format(min_date, "1%Y%m%d") & CALENDAR_ID <= format(max_date, "1%Y%m%d") 
              & Time >= min_hour & Time <= max_hour
              & Schedule %in% schedule])
}