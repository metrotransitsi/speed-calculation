# Bus Speed App Calculations
In order to use operational AVL (Automatic Vehicle Locator) system data to 
summarize and visualize bus speeds along a route or road segment, Metro Transit
(Minneapolis, MN) uses a custom set of functions defined here. 

The general process is:  

1. obtain street segment of interest (subset of pattern line shapefile)  
1. clean, optionally filter, and summarize raw AVL messages from operational source  
2. gather additional reference data like stop and traffic signal locations  
3. calculate point estimates of speed at each location and time  
4. interpolate speeds along segments based on observed speeds  
5. plot in a variety of ways:  
    
    * mean or other summary value along route line
    * "heatmap" of speed in space-time grid
    * biplot of speed versus distance traveled along route
    * biplot of speed versus time elapsed during service
    
At Metro Transit these functions are used to power an interactive Shiny app that
staff can use to identify a segment of interest, extract data from the operational
data store, and plot and explore resulting speed information for any route(s). 

## Using these functions  
This repository is self-contained with the data and functions necessary to run 
the calculations and visualizations. The user can clone the repository, then run 
the `calc-and-viz-speed.R` script in interactive mode to see how the functions work.

The functions are defined in the `/functions` directory and naming corresponds to the numbered
sequence of calls in the main script.

Data (`/Data`) provided are for three days of actual AVL bus records from a high-frequency local
bus route (Metro Transit Route 2). Also provided are a road segment of interest 
(along Franklin Ave in Minneapolis), a table of stop locations, and a subset of intersections
with traffic signals along the route. 

# Authorship and use
These functions were developed by Kim Eng Ky (kykimeng@gmail.com) while at Metro Transit.

These functions and the data used in the demonstration of their use are provided
in the hopes that other transit agencies can benefit from their creation, and build 
upon them. 

With questions contact Eric Lind (Eric.Lind@metrotransit.org).